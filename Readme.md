# What this is

Welcome to my /archinstall/ repo. I have created these config files, to install
to get a solid Linux desktop, with working Bluetooth and internet out of the box.

## Main Features

Use this config for the arch installer for an automatic installation of Arch Linux. The main features of this configuration file are:

- BTRFS file system with snapshots and multiple partitions.
- LUKS disk encryption with the possibility for hardware tokens.
- Choose between proprietary and open source drivers.
- Custom commands include optional /paru/ and sudoless docker support.
- Uses Gnu Skell to automatically setup dot files.

## Start here to learn about this setup

- [Arch Linux](https://archlinux.org/)
- [Packages](https://archlinux.org/packages/)
- [Forums](https://bbs.archlinux.org/)
- [Wiki](https://wiki.archlinux.org/)
- [Bugs](https://bugs.archlinux.org/)
- [Security](https://security.archlinux.org/)
- [AUR](https://aur.archlinux.org/)
- [Download](https://archlinux.org/download/)

## Word of advice

**Warning:**

- _archinstall_ stores all user and (secondary) disk encryption
  passwords in plain text.
  [\[1\]](https://github.com/archlinux/archinstall/issues/1111)
  [\[2\]](https://github.com/archlinux/archinstall/issues/1062)
  This is why the /creds.json/ is added to the .gitignore
- _archinstall_ offers different defaults than the regular
  [installation](https://wiki.archlinux.org/title/Installation_guide)
  process. When using a system installed with archinstall, please
  mention so in support requests and provide
  `/var/log/archinstall/install.log`.

## Running the installer

First, acquire and [boot the live medium](https://wiki.archlinux.org/title/Installation_guide#Boot_the_live_environment)
as described in [Installation guide#Pre-installation](https://wiki.archlinux.org/title/Installation_guide#Pre-installation).
The [archinstall](https://archlinux.org/packages/?name=archinstall) package is
part of the live medium and can be run directly:

## Using /archinstall/

The guided installer will perform (query) the following steps:

1.  configure the [Locale](https://wiki.archlinux.org/title/Locale);
2.  select the [Mirrors](https://wiki.archlinux.org/title/Mirrors);
3.  [Partition](https://wiki.archlinux.org/title/Partition) the disks;
4.  [Format](https://wiki.archlinux.org/title/Format) the partitions;
5.  enable [disk encryption](https://wiki.archlinux.org/title/Disk_encryption)
6.  set the [hostname](https://wiki.archlinux.org/title/Hostname);
7.  set the root password;
8.  install a [bootloader](https://wiki.archlinux.org/title/Boot_loader).

**Warning:** Leaving the root password blank disables the root account,
using [sudo](https://wiki.archlinux.org/title/Sudo) for privilege escalation. As this may
cause you to lock yourself out of your system, this is generally not
advised. See [Sudo#Disable root login](https://wiki.archlinux.org/title/Sudo#Disable_root_login).
:::

**Note:** The installer can configure wired interfaces on the installed
system using
[Systemd-networkd](https://wiki.archlinux.org/title/Systemd-networkd) or it can
copy the configuration used on the ISO. That means, if you configure a
wireless interface using [Iwd#iwctl](https://wiki.archlinux.org/title/Iwd#iwctl), its
configuration, including the network\'s password, will be copied to the
installed system. It also copies the configuration for wired interfaces
that\'s present on the ISO.
:::

Additional packages can be installed by specifying them after the
`Write additional packages to install` prompt.

Once the installation is complete, green text should appear saying that
it's safe to reboot, which is also the command you use to reboot.

### [Profiles]

archinstall includes [profiles](https://gitlab.archlinux.org/archlinux/archinstall/-/tree/master/profiles)
, or sets of packages and pre-configured options
which can be installed next to the base system.

**Warning:** Shipped profiles are specific to _archinstall_ and not
supported by package maintainers. Users are advised to check the details
of each profile before using it.
:::

## See also

- [python-archinstall: guided](https://archinstall.readthedocs.io/installing/guided.html)
